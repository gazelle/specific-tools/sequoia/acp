
# ACP workflow

## Overview

ACP is a workflow used by Sequoia.

## Install libraries required by SoapUI

Two external libraries are needed for the script to run: 

* [sequoia-security-adaptator](https://gazelle.ihe.net/nexus/#nexus-search;quick~sequoia-security-adaptor-jar) to generate the SAML assertion
* [Postgresql driver](https://gazelle.ihe.net/nexus/#nexus-search;gav~org.postgresql~postgresql~~~) to connect to the Patient Manager database (to retrieve application preferences needed to generate the SAML assertion)

To install them put them in :

* <SOAPUI_INSTALL_DIRECTORY>/lib/ when using the project in SoapUI GUI
* /usr/local/jboss7/ext when using the project in Gazelle Webservice Tester
